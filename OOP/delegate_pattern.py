class A:

    def __init__(self, a):
        self.a = a

    def pr(self):
        print('self.pr() call \n__bases__ class: %s\n__name__ class = %s\n self.a = %s ' % (
            self.__class__.__bases__, self.__class__.__name__, self.a))


class B:
    def __init__(self, b):
        self.a = A(a=b)

    def bpr(self):
        print('Method bpr')
        self.a.pr()
        print('delegate method pr from class A and do something else')


if __name__ == '__main__':

    a = A(a=3)
    b = B(b=5)
    a.pr()
    b.bpr()