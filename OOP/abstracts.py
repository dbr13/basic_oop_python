

from abc import ABCMeta, abstractmethod


class FirstAbstract(metaclass=ABCMeta):

    @abstractmethod
    def foo(self):
        pass


class XMLResponse(FirstAbstract):

    def foo(self):
        print('XMLResponse.foo()')


class JSONREsponse(FirstAbstract):

    def foo(self):
        print('JSONREsponse.foo()')


class APIResponse(FirstAbstract):

    def __init__(self, response_type):
        self.response_type = response_type
        # self._proxy_obj = None

    # def get_proxy_obj(self):
    #     if self.response_type == 'xml':
    #         self._proxy_obj = XMLResponse()
    #     else:
    #         self._proxy_obj = JSONREsponse()
    #     return self._proxy_obj

    @property
    def proxy_obj(self):
        if self.response_type == 'xml':
            return XMLResponse()
        return JSONREsponse()

    # @proxy_obj.setter
    # def proxy_obj(self, value):
    #     self._proxy_obj = value

    def foo(self):
        return self.proxy_obj.foo()

if __name__ == '__main__':

    # a = XMLResponse()
    # b = JSONREsponse()
    #
    # XMLResponse().foo()
    # a.foo()
    # JSONREsponse().foo()

    api_response = APIResponse(response_type='json')
    api_response.foo()
    # print(api_response._proxy_obj)
    api_response.response_type = 'xml'
    api_response.foo()
    print(api_response.proxy_obj)
    # api_response.proxy_obj = 'aaaa'
    # print(api_response._proxy_obj)
