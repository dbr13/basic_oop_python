

class A:

    def __init__(self, a, b, p):
        self.p = p             # public
        self._a = a            # protected var
        self.__b = b           # private var

    def _foo(self):
        print('Protected method _foo')

    def __foo(self):
        print('private method __foo')


class B(A):
    pass


class C:

    def __init__(self, a, b, p):
        self.a = A(a=a, b=b, p=p)


if __name__ == '__main__':

    a = A(a='A._a', b='A.__b', p='A.p')

    b = B(a='B.a', b='B.b', p='B.p')

    c = C(a='C.a', b='C.b', p='C.p')

    print(a._a + '\n' + a._A__b + '\n' + a.p)
    a._foo()
    a._A__foo()

    print(b._a + '\n' + b._A__b + '\n' + b.p)

    b._foo()
    b._A__foo()

    print(c.a._a + '\n' + c.a._A__b + '\n' + c.a.p)

    c.a._foo()
    c.a._A__foo()