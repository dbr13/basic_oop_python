
class A:

    def __init__(self, a):
        self.a = a

    def pr(self):
        print('self.pr() call \n__bases__ class: %s\n__name__ class = %s\n self.a = %s ' % (
            self.__class__.__bases__, self.__class__.__name__, self.a))


class B(A):
    def __init__(self, a):
        super(B, self).__init__(a=a)

    def bpr(self):
        print('self.bpr')

    # def pr(self):
    #     A.pr(self)

class C(B):
    pass

    def pr(self):
        B.pr(self)
        print('Pereopredelili metod pr')

    def pr_1(self):
        print('class C method.pr')

class D(C, B):
    pass

    def pr(self):
        B.pr(self)


if __name__ == '__main__':

    a = A(a=3)
    b = B(a=5)
    c = C(a=8)
    d = D(a=9)
    a.pr()
    b.pr()
    c.pr()
    d.pr()
    d.bpr()
    d.pr_1()


# self.pr() call
# __bases__ class: (<class 'object'>,)
# __name__ class = A
#  self.a = 3
# self.pr() call
# __bases__ class: (<class '__main__.A'>,)
# __name__ class = B
#  self.a = 5
# self.pr() call
# __bases__ class: (<class '__main__.B'>,)
# __name__ class = C
#  self.a = 8
# Pereopredelili metod pr
#self.pr() call
# __bases__ class: (<class '__main__.C'>, <class '__main__.A'>)
# __name__ class = D
#  self.a = 9
# Pereopredelili metod pr
